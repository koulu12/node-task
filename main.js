const express = require("express");
const app = express();
const PORT = 3000;

//build small REST API with Express

console.log("Server-side program starting...")

// baseurl: http://localgost:3000
//Endpoiint: http://localgost:3000/
app.get('/', (req,res) => {
    res.send('Hello world!!')
})

/** 
* This arrow function adds two numbers together
* @param {Number} a first param
* @param {Number} b second param
* @returns {Number}
*/
const add = (a,b) => {
    const sum = a + b;
    return sum;
}

// Adding endpoint: http://localhost:3000/add?a=value&b=value
app.get('/add', (req,res) => {
    console.log(req.query);
    const a = parseInt(req.query.a);
    const b = parseInt(req.query.b);
    if(isNaN(a) || isNaN(b)) {
        res.send("a and b must be numbers");
    } else {
        const sum = add(a, b);
        res.send(sum.toString());
    }
})

app.listen(PORT, () => console.log(
    `Server listening http://localhost:${PORT}`
))